INTRODUCTION
------------
This module creates a teaser in a block with content created some time ago.

Using a time frame, defined on the modules configuration page, the module
increases the chance of having a teaser with enough content to reach the
defined limit by first getting content after the "Start Date" until the "End 
Date". If that doesn't fill up the desired "Maximum number of links" another 
request will get content from before the "Start Date". That way your teaser 
won't be half empty all the time, which would be the case if you don't write 
content on a daily basis, and the teaser will contain content created some 
time ago...

Settings: admin/config/content/sometimeago

- Start Date: How many days ago should the content approximately have been 
  published.
- End Date: Allowed range of days forward in time.
- Select the content type to link to.
- Maximum number of links to content.
- Sort content by date created.
- Enable cache.

A default "somtimeago"-block will max 10 article nodes from 365 days ago
with a range of 30 days. Cache is disabled by default.

INSTALLATION
------------
Install as you would normally install a contributed drupal module.

CONFIGURATION
-------------
Configure the module on the configuration page: 
Home » Administration » Configuration » Modules » Some time ago configuration 
(admin/config/content/sometimeago)

MAINTAINERS
-----------
* thelynge - https://drupal.org/user/2703525
