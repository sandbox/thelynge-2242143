<?php
/**
 * @file sometimeago-block.tpl.php
 * Content block
 *
 * Variables available:
 * - $nodes: An array containing nodes and node links.
 */
?>
<div class="sometimeago-container">
    <ul>
    <?php

        foreach ($nodes as $node) {
            echo '<li>' . $node['link'] . '</li>';
        }

    ?>
    </ul>
</div>
