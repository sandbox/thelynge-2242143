<?php

/**
 * @file
 * Hooks to setup a configuration page.
 */

/**
 * Setup the form on the configuration page.
 */
function sometimeago_admin_settings() {
  $form = array();

  $form['sometimeago_days'] = array(
    '#type' => 'textfield',
    '#title' => 'Start date',
    '#description' => 'How many days ago should the content approximately have been published',
    '#default_value' => variable_get('sometimeago_days', SOMETIMEAGO_DEFAULT_DAYS),
    '#size' => 4,
  );

  $form['sometimeago_range'] = array(
    '#type' => 'textfield',
    '#title' => 'End date',
    '#description' => 'Allowed range of days forward in time: (Start Date + End Date)',
    '#default_value' => variable_get('sometimeago_range', SOMETIMEAGO_DEFAULT_RANGE),
    '#size' => 4,
  );

  $arr_content_type_options = array();
  $arr_content_types = node_type_get_types();
  foreach ($arr_content_types as $obj_content_type) {
    $arr_content_type_options[$obj_content_type->type] = $obj_content_type->name;
  }
  $form['sometimeago_contenttype'] = array(
    '#type' => 'select',
    '#title' => 'Select the content type to link to',
    '#options' => $arr_content_type_options,
    '#default_value' => variable_get('sometimeago_contenttype', SOMETIMEAGO_DEFAULT_CONTENTTYPE),
  );

  $form['sometimeago_limit'] = array(
    '#type' => 'textfield',
    '#title' => 'Maximum number of links to content',
    '#default_value' => variable_get('sometimeago_limit', SOMETIMEAGO_DEFAULT_LIMIT),
    '#size' => 2,
    '#maxlength' => 2,
  );

  $form['sometimeago_sort'] = array(
    '#type' => 'select',
    '#title' => 'Sort content by date created',
    '#options' => array('ASC' => 'Ascending', 'DESC' => 'Descending'),
    '#default_value' => variable_get('sometimeago_sort', SOMETIMEAGO_DEFAULT_SORT),
  );

  $form['sometimeago_cache'] = array(
    '#type' => 'checkbox',
    '#title' => 'Cache',
    '#default_value' => variable_get('sometimeago_cache', SOMETIMEAGO_DEFAULT_CACHE),
  );

  return system_settings_form($form);
}

/**
 * Validate integers entered in fields on configuration page.
 */
function sometimeago_admin_validate($form, &$form_state) {
  $limit = $form_state['values']['sometimeago_limit'];
  if (!is_numeric($limit)) {
    form_set_error('sometimeago_limit', t('You must enter an integer for the maximum number of links.'));
  }
  elseif ($limit < 0) {
    form_set_error('sometimeago_limit', t('Maximum number of links must be a positive number.'));
  }

  $days = $form_state['values']['sometimeago_days'];
  if (!is_numeric($days)) {
    form_set_error('sometimeago_days', t('You must enter the days as a number.'));
  }
  else {
    form_set_error('sometimeago_days', t('Number of days must be a positive number.'));
  }

  $range = $form_state['values']['sometimeago_range'];
  if (!is_numeric($range)) {
    form_set_error('sometimeago_range', t('You must enter the days as a number.'));
  }
  else {
    form_set_error('sometimeago_range', t('Number of days must be a positive number.'));
  }
}
